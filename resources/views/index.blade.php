@extends('komponen.app')
@section('konten')
    {{-- title konten --}}
    <div class="caraosel-container">
        @include('komponen.carousel.carousel-content')
    </div>

    {{-- title konten --}}
    <div class="body-title">
        <h1 class="text-uppercase mt-2">Produk Baru</h1>
    </div>
    {{-- card --}}
    <div class="body-container">
        <div class="row">
            @foreach ($data as $item)
                <div class="col-sm-3 mb-4">
                    <div class="card fw-bolder">
                        <a href="{{ route('products.show', $item->id) }}" class="text-center">
                            <div class="card-header">
                                <h5 class="card-title text-uppercase mt-2">{{ $item->nama_produk }}</h5>
                            </div>
                            <img src="{{ asset('storage/' . $item->gambar) }}" class="card-img-top"
                                alt="{{ $item->nama_produk }}">
                            <div class="card-body text-start">
                                <p class="card-text">Harga Rp.{{ number_format($item->harga, 0, ',', '.') }}</p>
                                <p class="card-text"><small class="text-muted">Stock {{ $item->qty }}</small></p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
