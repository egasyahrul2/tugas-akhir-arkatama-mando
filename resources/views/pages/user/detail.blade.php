@extends('komponen.app')
@section('konten')
    {{-- title konten --}}
    <div class="body-title">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-uppercase mt-2">Detail Produk</h1>
            </div>
            <div class="col-md-6">
                <h4 class="text-end mt-3"><a href="/">Home</a> / <a href="/barang">Produk</a> / {{ $data->nama_produk }}
                </h4>
            </div>
        </div>
    </div>

    <div class="container mt-4">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('storage/' . $data->gambar) }}" class="img-fluid" alt="{{ $data->nama_produk }}">
            </div>
            <div class="col-md-6">
                <button type="button" class="btn btn-secondary">
                    <a href="javascript:history.back()" class="text-white" style="text-decoration: none">
                        <i class="fas fa-long-arrow-alt-left"></i> Kembali</a>
                </button>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <h1>{{ $data->nama_produk }}</h1>
                <h2>Rp.{{ number_format($data->harga, 0, ',', '.') }}</h2>
                <p>{{ $data->deskripsi }}, berwarna {{ $data->warna }}</p>
                @if (Auth::check())
                    <form action="{{ route('cart.store', $data->id) }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-primary">
                            <i class="fas fa-cart-plus"></i> Tambah ke Keranjang
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
