@extends('komponen.app')
@section('konten')
    {{-- title konten --}}
    <div class="body-title">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-uppercase mt-2">Topup</h1>
            </div>
            <div class="col-md-6">
                <h4 class="text-end mt-3"><a href="/">Home</a> / Profil / Topup
                </h4>
            </div>
        </div>
    </div>

    <!-- Alert content -->
    <div class="container alert-container">
        <div class="d-flex align-items-center justify-content-between">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        <div class="d-flex align-items-center justify-content-between">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </div>

    <div class="body-container">
        <div class="row row-cols-1 row-cols-md-3 my-3 text-center">
            <div class="col">
                <div class="card mb-4 rounded-3 shadow-sm border-success">
                    <div class="card-header py-3">
                        <h4 class="my-0 fw-normal">Paket 1</h4>
                    </div>
                    <div class="card-body">
                        <h1 class="card-title pricing-card-title">Rp.50.000</h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Kamu akan mendapatkan</li>
                            <li>saldo senilai</li>
                            <li>Rp.50.000</li>
                        </ul>
                        <form action="{{ route('topup.buyPackage', 50000) }}" method="POST">
                            @csrf
                            <button type="submit" class="w-100 btn btn-lg btn-success"><i class="fas fa-shopping-cart"></i>
                                Beli Paket</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card mb-4 rounded-3 shadow-sm border-success">
                    <div class="card-header py-3">
                        <h4 class="my-0 fw-normal">Paket 2</h4>
                    </div>
                    <div class="card-body">
                        <h1 class="card-title pricing-card-title">Rp.100.000</h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Kamu akan mendapatkan</li>
                            <li>saldo senilai</li>
                            <li>Rp.100.000</li>
                        </ul>
                        <form action="{{ route('topup.buyPackage', 100000) }}" method="POST">
                            @csrf
                            <button type="submit" class="w-100 btn btn-lg btn-success"><i class="fas fa-shopping-cart"></i>
                                Beli Paket</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card mb-4 rounded-3 shadow-sm border-success">
                    <div class="card-header py-3">
                        <h4 class="my-0 fw-normal">Paket Sultan</h4>
                    </div>
                    <div class="card-body">
                        <h1 class="card-title pricing-card-title">Rp.500.000</h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Kamu akan mendapatkan</li>
                            <li>saldo senilai</li>
                            <li>Rp.500.000</li>
                        </ul>
                        <form action="{{ route('topup.buyPackage', 500000) }}" method="POST">
                            @csrf
                            <button type="submit" class="w-100 btn btn-lg btn-success"><i class="fas fa-shopping-cart"></i>
                                Beli Paket</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
