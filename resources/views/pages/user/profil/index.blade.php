@extends('komponen.app')
@section('konten')
    {{-- title konten --}}
    <div class="body-title">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-uppercase mt-2">Profil</h1>
            </div>
            <div class="col-md-6">
                <h4 class="text-end mt-3"><a href="/">Home</a> / Profil
                </h4>
            </div>
        </div>
    </div>

    <!-- Alert content -->
    <div class="container alert-container">
        <div class="d-flex align-items-center justify-content-between">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        <div class="d-flex align-items-center justify-content-between">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </div>

    <div class="body-container">
        <div class="row">
            <div class="col-md-3">
                <img src="{{ asset('assets/img/custom.jpg') }}" class="img-fluid" alt="{{ $user->name }}">
            </div>
            <div class="col-md-9">
                {{-- <button type="button" class="btn btn-secondary">
                    <a href="javascript:history.back()" class="text-white" style="text-decoration: none">
                        <i class="fas fa-long-arrow-alt-left"></i> Kembali</a>
                </button> --}}
                {{-- <form id="editUserForm" action="/profil/{id}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT') --}}
                <input type="hidden" id="user_id" name="user_id">
                <div class="mb-3">
                    <label for="nama_user" class="form-label">Nama User<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" value="{{ $user->name }}" id="nama_user" name="nama_user"
                        disabled>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" value="{{ $user->email }}" id="email" name="email"
                        disabled>
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">Alamat<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" value="{{ $user->alamat }}" id="alamat" name="alamat"
                        disabled>
                </div>
                <div class="mb-3">
                    <label for="saldo" class="form-label">Saldo</label>
                    <input type="text" class="form-control" value="Rp.{{ number_format($user->saldo, 0, ',', '.') }}"
                        id="saldo" name="saldo" disabled>
                </div>
                {{-- <button type="submit" class="btn btn-primary">Simpan</button>
                </form> --}}
                <a href="{{ route('topup.index') }}">
                    <button class="btn btn-primary">
                        <i class="fas fa-plus fs-3 me-2"></i>
                        <span class="ms-2 fs-5">TopUp</span>
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        // $('#editProductForm').on('submit', function(e) {
        //     e.preventDefault();
        //     var user_id = $('#user_id').val();
        //     var formData = new FormData(this);
        //     formData.append('_method', 'PUT');

        //     $.ajax({
        //         url: '/profil/' + user_id,
        //         method: 'POST',
        //         data: formData,
        //         contentType: false,
        //         processData: false,
        //         headers: {
        //             'X-HTTP-Method-Override': 'PUT'
        //         },
        //         success: function(response) {
        //             alert(response.success);
        //         },
        //         error: function(response) {
        //             if (response.responseJSON && response.responseJSON.errors && response
        //                 .responseJSON.errors.gambar) {
        //                 alert(response.responseJSON.errors.gambar[0]);
        //             } else {
        //                 alert('Data Gagal Ditambahkan');
        //             }
        //         }
        //     });
        // });
    </script>
@endpush
