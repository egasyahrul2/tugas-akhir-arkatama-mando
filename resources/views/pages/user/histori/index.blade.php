@extends('komponen.app')
@section('konten')
    {{-- title konten --}}
    <div class="body-title">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-uppercase mt-2">Histori</h1>
            </div>
            <div class="col-md-6">
                <h4 class="text-end mt-3"><a href="/">Home</a> / Histori</h4>
            </div>
        </div>
    </div>

    <div class="body-container">
        @if ($products->isEmpty())
            <div class="alert alert-warning text-center">
                <h4>Data kosong. Klik <a href="{{ route('products.home') }}">di sini</a> untuk lanjut belanja.</h4>
            </div>
        @else
            <div class="row mb-2">
                @foreach ($products as $item)
                    <div
                        class="card card-items row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col-auto d-none d-lg-block">
                            <img class="bd-placeholder-img" width="250" height="250"
                                src="{{ asset('storage/' . $item->product->gambar) }}"
                                alt="{{ $item->product->nama_produk }}">
                        </div>
                        <div class="col p-4 d-flex flex-column position-static">
                            <h1 class="mb-0 fw-bolder">{{ $item->product->nama_produk }}</h1>
                            <div class="mb-1 text-muted fs-4">Total Rp.{{ number_format($item->total, 0, ',', '.') }},
                                Quantity
                                {{ $item->qty }} buah</div>
                            <p class="card-text mb-auto fs-5">{{ $item->product->deskripsi }}.</p>
                            <a href="{{ route('products.show', $item->product->id) }}">
                                <button class="btn btn-success">
                                    <i class="fas fa-shopping-cart"></i> Beli lagi
                                </button>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection
