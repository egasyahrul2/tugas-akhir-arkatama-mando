@extends('komponen.app')
@section('konten')
    {{-- title konten --}}
    <div class="body-title">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-uppercase mt-2">Keranjang</h1>
            </div>
            <div class="col-md-6">
                <h4 class="text-end mt-3"><a href="/">Home</a> / Keranjang
                </h4>
            </div>
        </div>
    </div>

    <!-- Alert content -->
    <div class="container alert-container">
        <div class="d-flex align-items-center justify-content-between">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        <div class="d-flex align-items-center justify-content-between">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </div>

    <!-- DataTable -->
    <div class="table-responsive">
        {{ $dataTable->table() }}
    </div>
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    <script type="text/javascript"></script>
@endpush
