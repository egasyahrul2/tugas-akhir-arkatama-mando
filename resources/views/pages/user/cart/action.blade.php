<div class="d-flex justify-content-center align-items-center gap-2">
    <form action="{{ route('cart.add', $data->id) }}" method="POST" class="mt-2">
        @csrf
        <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i></i></button>
    </form>
<div class="d-flex justify-content-center align-items-center gap-2">
    <form action="{{ route('cart.update', $data->id) }}" method="POST" class="mt-2">
        @csrf
        <button type="submit" class="btn btn-success"><i class="fas fa-dollar-sign"></i></button>
    </form>
<div class="d-flex justify-content-center align-items-center gap-2">
    <form action="{{ route('cart.destroy', $data->id) }}" method="POST" class="mt-2">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger"><i class="fas fa-minus"></i></button>
    </form>
</div>
