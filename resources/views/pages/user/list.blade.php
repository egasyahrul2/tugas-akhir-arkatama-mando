@extends('komponen.app')
@section('konten')
    {{-- title konten --}}
    <div class="body-title">
        <div class="row">
            <div class="col-md-3">
                <h1 class="text-uppercase mt-2">List Produk</h1>
            </div>
            <div class="col-md-6">
                <div class="search-bar mt-3">
                    <form action="{{ route('products.home') }}" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control border border-dark" name="search" aria-describedby="addon-button" placeholder="Cari Produk" value="{{ request('search') }}">
                            <button type="submit" class="btn btn-secondary border-dark" id="addon-button"><i class="fas fa-search"></i> Cari</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3">
                <h4 class="text-end mt-3"><a href="/" style="">Home</a> / Produk</a></h4>
            </div>
        </div>
    </div>
    {{-- card --}}
    <div class="body-container">
        <div class="row">
            @foreach ($data as $item)
                <div class="col-sm-3 mb-4">
                    <div class="card fw-bolder">
                        <a href="{{ route('products.show', $item->id) }}" class="text-center">
                            <div class="card-header">
                                <h5 class="card-title text-uppercase mt-2">{{ $item->nama_produk }}</h5>
                            </div>
                            <img src="{{ asset('storage/' . $item->gambar) }}" class="card-img-top"
                                alt="{{ $item->nama_produk }}">
                            <div class="card-body text-start">
                                <p class="card-text">Harga Rp.{{ number_format($item->harga, 0, ',', '.') }}</p>
                                <p class="card-text"><small class="text-muted">Stock {{ $item->qty }}</small></p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
