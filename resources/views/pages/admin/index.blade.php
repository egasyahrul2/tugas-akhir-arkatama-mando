@extends('komponen.app-admin')
@section('isi')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Home Page</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Home</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Card: Total Pesanan -->
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{ $pending }}</h3>
                            <p>Pesanan Pending</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-exclamation"></i>
                        </div>
                        <a href="{{ route('order.index') }}" class="small-box-footer">Info Lebih Lanjut <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- Card: Total Laba -->
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>Rp. {{ number_format($totalLaba, 0, ',', '.') }}</h3>
                            <p>Total Laba</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-dollar-sign"></i>
                        </div>
                        <a href="{{ route('history.index') }}" class="small-box-footer">Info Lebih Lanjut <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- Card: Total Users -->
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner text-white">
                            <h3>{{ $totalUsers }} Orang</h3>
                            <p>Total Users</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-users"></i>
                        </div>
                        <a href="{{ route('users.index') }}" class="small-box-footer"><span class="text-white">Info Lebih
                                Lanjut </span><i class="fas fa-arrow-circle-right text-white"></i></a>
                    </div>
                </div>

                <!-- Card: Total Produk -->
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $totalProducts }}</h3>
                            <p>Total Produk</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-box"></i>
                        </div>
                        <a href="{{ route('produk.index') }}" class="small-box-footer">Info Lebih Lanjut <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- Card: Total Barang Terjual -->
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $totalBarangTerjual }}</h3>
                            <p>Total Barang Terjual</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-shopping-cart"></i>
                        </div>
                        <a href="{{ route('history.index') }}" class="small-box-footer">Info Lebih Lanjut <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection
