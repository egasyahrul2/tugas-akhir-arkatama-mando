@extends('komponen.app-admin')
@section('isi')
    <!-- Content-header -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Halaman Produk</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Produk</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <div class="card-body py-4">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-flex gap-1 mb-3">
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addProductModal">
                    <i class="fas fa-plus fs-3 me-2"></i>
                    <span class="ms-2 fs-5">Tambah</span>
                </button>
            </div>
        </div>

        <!-- Modal -->
        @include('pages.admin.product.modal')

        <!-- DataTable -->
        <div class="table-relative">
            {{ $dataTable->table() }}
        </div>
    </div><!-- /.content -->
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    <script type="text/javascript">
        $(function() {
            var table = $('#product-table').DataTable();

            $('#addProductForm').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: '{{ route('produk.store') }}',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        $('#addProductModal').modal('hide');
                        table.ajax.reload();
                        alert(response.success);
                    },
                    error: function(response) {
                        if (response.responseJSON && response.responseJSON.errors && response
                            .responseJSON.errors.gambar) {
                            alert(response.responseJSON.errors.gambar[0]);
                        } else {
                            alert('Data Gagal Ditambahkan');
                        }
                    }
                });
            });

            $('#gambar').on('change', function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview_gambar').attr('src', e.target.result)
                        .show();
                }
                reader.readAsDataURL(this.files[0]);
            });

            $(document).on('click', '.btn-edit', function() {
                var productId = $(this).data('id');

                $.ajax({
                    url: '/produk/' + productId + '/edit',
                    method: 'GET',
                    success: function(response) {
                        $('#edit_product_id').val(productId);
                        $('#edit_nama_produk').val(response.product.nama_produk);
                        $('#edit_preview_gambar').attr('src', response.product.gambar_url).show();
                        $('#edit_gambar').val('');
                        $('#edit_qty').val(response.product.qty);
                        $('#edit_harga').val(response.product.harga);
                        $('#edit_warna').val(response.product.warna);
                        $('#edit_deskripsi').val(response.product.deskripsi);

                        var categories = response.categories;
                        var categoryDropdown = $('#edit_id_kategori');
                        categoryDropdown.empty();
                        $.each(categories, function(index, category) {
                            categoryDropdown.append($('<option>', {
                                value: category.id,
                                text: category.nama_kategori,
                                selected: category.id == response.product
                                    .id_kategori
                            }));
                        });

                        $('#editProductModal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText);
                    }
                });
            });

            $('#edit_gambar').on('change', function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#edit_preview_gambar').attr('src', e.target.result)
                        .show();
                }
                reader.readAsDataURL(this.files[0]);
            });

            $('#editProductForm').on('submit', function(e) {
                e.preventDefault();
                var productId = $('#edit_product_id').val();
                var formData = new FormData(this);
                formData.append('_method', 'PUT');

                $.ajax({
                    url: '/produk/' + productId,
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-HTTP-Method-Override': 'PUT'
                    },
                    success: function(response) {
                        $('#editProductModal').modal('hide');
                        table.ajax.reload();
                        alert(response.success);
                    },
                    error: function(response) {
                        if (response.responseJSON && response.responseJSON.errors && response
                            .responseJSON.errors.gambar) {
                            alert(response.responseJSON.errors.gambar[0]);
                        } else {
                            alert('Data Gagal Ditambahkan');
                        }
                    }
                });
            });

            $(document).on('click', '.btn-delete', function() {
                var productId = $(this).data('id');

                if (confirm('Apakah Anda yakin ingin menghapus produk ini?')) {
                    $.ajax({
                        url: '/produk/' + productId,
                        method: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(response) {
                            alert(response.success);
                            table.ajax.reload();
                        },
                        error: function(xhr, status, error) {
                            console.error(xhr.responseText);
                        }
                    });
                }
            });

            $('#searchBox').on('keyup', function() {
                table.search(this.value).draw();
            });
        });
    </script>
@endpush
