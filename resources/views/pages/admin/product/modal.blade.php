<!-- Add Modal -->
<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="addProductModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addProductModalLabel">Tambah Produk</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="addProductForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="edit_product_id" name="product_id">
                    <div class="mb-3">
                        <label for="nama_produk" class="form-label">Nama Produk<span
                                class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="nama_produk" name="nama_produk" required>
                    </div>
                    <div class="mb-3">
                        <label for="id_kategori" class="form-label">Kategori<span class="text-danger">*</span></label>
                        <select class="form-control" id="id_kategori" name="id_kategori" required>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->nama_kategori }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label for="preview_gambar">Pratinjau Gambar</label>
                        <img id="preview_gambar" src="#" alt="Gambar Produk" class="img-fluid"
                            style="display: none;" />
                    </div>
                    <div class="mb-3">
                        <label for="gambar" class="form-label">Gambar<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" id="gambar" name="gambar" required>
                    </div>
                    <div class="mb-3">
                        <label for="qty" class="form-label">Quantity<span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="qty" name="qty" required>
                    </div>
                    <div class="mb-3">
                        <label for="harga" class="form-label">Harga Produk<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="harga" name="harga" required>
                    </div>
                    <div class="mb-3">
                        <label for="warna" class="form-label">Warna Produk<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="warna" name="warna" required>
                    </div>
                    <div class="mb-3">
                        <label for="deskripsi" class="form-label">Deskripsi</label>
                        <input type="text" class="form-control" id="deskripsi" name="deskripsi">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit Product -->
<div class="modal fade" id="editProductModal" tabindex="-1" aria-labelledby="editProductModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProductModalLabel">Edit Product</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="editProductForm" action="/produk/{id}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="edit_product_id" name="product_id">
                    <div class="mb-3">
                        <label for="edit_nama_produk" class="form-label">Nama Produk</label>
                        <input type="text" class="form-control" id="edit_nama_produk" name="nama_produk" required>
                    </div>
                    <div class="mb-3">
                        <label for="edit_id_kategori" class="form-label">Kategori</label>
                        <select class="form-control" id="edit_id_kategori" name="id_kategori" required>
                            <!-- Options akan diisi oleh JavaScript -->
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label for="edit_preview_gambar">Pratinjau Gambar</label>
                        <img id="edit_preview_gambar" src="#" alt="Gambar Produk" class="img-fluid" style="display: none;" />
                    </div>
                    <div class="mb-3">
                        <label for="edit_gambar" class="form-label">Gambar</label>
                        <input type="file" class="form-control" id="edit_gambar" name="gambar">
                    </div>
                    <div class="mb-3">
                        <label for="edit_qty" class="form-label">Quantity</label>
                        <input type="number" class="form-control" id="edit_qty" name="qty" required>
                    </div>
                    <div class="mb-3">
                        <label for="edit_harga" class="form-label">Harga Produk</label>
                        <input type="text" class="form-control" id="edit_harga" name="harga" required>
                    </div>
                    <div class="mb-3">
                        <label for="edit_warna" class="form-label">Warna Produk</label>
                        <input type="text" class="form-control" id="edit_warna" name="warna" required>
                    </div>
                    <div class="mb-3">
                        <label for="edit_deskripsi" class="form-label">Deskripsi</label>
                        <input type="text" class="form-control" id="edit_deskripsi" name="deskripsi">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
