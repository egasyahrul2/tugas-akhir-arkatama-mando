<div class="d-flex justify-content-center align-items-center gap-2">
    <button data-id="{{ $data->id }}" class="btn btn-warning btn-edit">
        <i class="fas fa-pen fs-2"></i>
    </button>
    &nbsp;
    <button data-id="{{ $data->id }}" class="btn btn-danger btn-delete">
        <i class="fas fa-trash fs-2"></i>
    </button>
</div>
