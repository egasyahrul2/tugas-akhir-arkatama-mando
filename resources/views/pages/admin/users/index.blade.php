@extends('komponen.app-admin')
@section('isi')
    <!-- Content-header -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Halaman Pengguna</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Pengguna</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Alert content -->
    <div class="container alert-container">
        <div class="d-flex align-items-center justify-content-between">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        <div class="d-flex align-items-center justify-content-between">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </div>

    <!-- Main content -->
    <div class="card-body py-4">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-flex gap-1 mb-3">
                <a href="{{ route('users.create') }}">
                    <button class="btn btn-primary">
                        <i class="fas fa-plus fs-3 me-2"></i>
                        <span class="ms-2 fs-5">Tambah</span>
                    </button>
                </a>
            </div>
        </div>

        <!-- DataTable -->
        <div class="table-responsive">
            {{ $dataTable->table() }}
        </div>
    </div><!-- /.content -->
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
@endpush
