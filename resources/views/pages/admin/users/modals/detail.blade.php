@extends('komponen.app-blank')
@section('konten')
    <!-- Content-header -->
    <div class="content-header bg-dark">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-1 text-center">
                    <a href="javascript:history.back()" class="text-white"><i class="fas fa-long-arrow-alt-left"></i>
                        Kembali</a>
                </div><!-- /.col -->
                <div class="col-sm-7 text-white">
                    <h1 class="m-0">Halaman Detail Pengguna</h1>
                </div><!-- /.col -->
                <div class="col-sm-4">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard" class="text-white">Home</a></li>
                        <li class="breadcrumb-item"><a href="/pengguna" class="text-white">Pengguna</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <div class="card-body py-4">
        <div class="d-flex align-items-center justify-content-between">
            <div class="container">
                <form action="{{ route('users.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="preview_gambar" class="form-label">Foto Profil</label><br>
                        <img id="preview_gambar"
                            src="{{ $data->gambar ? asset('storage/' . $data->gambar) : asset('assets/img/custom.jpg') }}"
                            alt="Foto Profil" class="img-fluid">
                    </div>
                    <div class="mb-3">
                        <label for="gambar" class="form-label">Upload Foto Profil</label>
                        <input type="file" class="form-control" id="gambar" name="gambar" disabled>
                    </div>
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $data->name }}"
                            required>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ $data->email }}"
                            required>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password<span class="text-danger">*</span></label>
                        <input type="password" class="form-control" id="password" name="password">
                        <small class="form-text text-muted">Kosongkan jika tidak ingin mengubah password</small>
                    </div>
                    <div class="mb-3">
                        <label for="alamat" class="form-label">Alamat<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $regis->alamat }}"
                            required>
                    </div>
                    <div class="mb-3">
                        <label for="role" class="form-label">Role</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="user" value="user"
                                {{ $data->role == 'user' ? 'checked' : '' }}>
                            <label class="form-check-label" for="user">User</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="admin" value="admin"
                                {{ $data->role == 'admin' ? 'checked' : '' }}>
                            <label class="form-check-label" for="admin">Admin</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection
