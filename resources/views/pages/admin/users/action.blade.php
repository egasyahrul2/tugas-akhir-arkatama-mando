<div class="d-flex justify-content-center align-items-center gap-2">
    <button class="btn btn-warning btn-edit">
        <a href="{{ route('users.edit', $data->id) }}">
            <i class="fas fa-pen fs-2"></i>
        </a>
    </button>
    &nbsp;<form action="{{ route('users.destroy', $data->id) }}" method="POST" class="mt-2">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger"><i class="fas fa-trash fs-2"></i></button>
    </form>
</div>
