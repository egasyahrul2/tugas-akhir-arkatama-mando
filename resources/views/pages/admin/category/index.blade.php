@extends('komponen.app-admin')
@section('isi')
    <!-- Content-header -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Halaman Kategori</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Kategori</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <div class="card-body py-4">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-flex gap-1 mb-3">
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addCategoryModal">
                    <i class="fas fa-plus fs-3 me-2"></i>
                    <span class="ms-2 fs-5">Tambah</span>
                </button>
            </div>
        </div>

        <!-- Modal -->
        @include('pages.admin.category.modal')

        <!-- DataTable -->
        <div class="table-relative">
            {{ $dataTable->table() }}
        </div>
    </div><!-- /.content -->
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    <script type="text/javascript">
        $(function() {
            var table = $('#category-table').DataTable();

            $('#addCategoryForm').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: '{{ route('kategori.store') }}',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        $('#addCategoryModal').modal('hide');
                        table.ajax.reload();
                        alert(response.success);
                    },
                    error: function(response) {
                        alert('Data Gagal Ditambahkan');
                    }
                });
            });

            $(document).on('click', '.btn-edit', function() {
                var categoryId = $(this).data('id');

                $.ajax({
                    url: '/kategori/' + categoryId + '/edit',
                    method: 'GET',
                    success: function(response) {
                        $('#edit_category_id').val(categoryId);
                        $('#edit_nama_kategori').val(response.nama_kategori);
                        $('#edit_deskripsi').val(response.deskripsi);
                        $('#editCategoryModal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText);
                    }
                });
            });

            $('#editCategoryForm').on('submit', function(e) {
                e.preventDefault();
                var categoryId = $('#edit_category_id').val();
                var formData = new FormData(this);
                formData.append('_method', 'PUT');

                $.ajax({
                    url: '/kategori/' + categoryId,
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-HTTP-Method-Override': 'PUT'
                    },
                    success: function(response) {
                        $('#editCategoryModal').modal('hide');
                        table.ajax.reload();
                        alert(response.success);
                    },
                    error: function(response) {
                        alert('Data Gagal Diubah');
                    }
                });
            });

            $(document).on('click', '.btn-delete', function() {
                var categoryId = $(this).data('id');

                if (confirm('Apakah Anda yakin ingin menghapus kategori ini?')) {
                    $.ajax({
                        url: '/kategori/' + categoryId,
                        method: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(response) {
                            alert(response.success);
                            table.ajax.reload();
                        },
                        error: function(xhr, status, error) {
                            console.error(xhr.responseText);
                        }
                    });
                }
            });

            $('#searchBox').on('keyup', function() {
                table.search(this.value).draw();
            });
        });
    </script>
@endpush
