<div class="d-flex justify-content-center align-items-center gap-2">
    <form action="{{ route('order.update', $data->id) }}" method="POST" class="mt-2">
        @csrf
        @method('PUT')
        <button type="submit" class="btn btn-success btn-edit">
            <i class="fas fa-check"></i> Konfirmasi</button>
    </form>
</div>
