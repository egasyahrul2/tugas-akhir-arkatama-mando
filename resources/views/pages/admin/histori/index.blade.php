@extends('komponen.app-admin')
@section('isi')
    <!-- Content-header -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Halaman Histori</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Histori</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Alert content -->
    <div class="container alert-container">
        <div class="d-flex align-items-center justify-content-between">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        <div class="d-flex align-items-center justify-content-between">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </div>

    <!-- Main content -->
    <div class="card-body py-4">
        <!-- DataTable -->
        <div class="table-relative">
            {{ $dataTable->table() }}
        </div>
    </div>
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    <script type="text/javascript">
        $(function() {
            var table = $('#history-table').DataTable();
            $('#searchBox').on('keyup', function() {
                table.search(this.value).draw();
            });
        });
    </script>
@endpush
