<!-- Add Modal -->
<div class="modal fade" id="addSliderModal" tabindex="-1" role="dialog" aria-labelledby="addSliderModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addSliderModalLabel">Tambah Slider</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="addSliderForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="edit_slider_id" name="slider_id">
                    <div class="mb-3">
                        <label for="nama_slider" class="form-label">Nama Slider<span
                                class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="nama_slider" name="nama_slider" required>
                    </div>
                    <div class="form-group mb-3">
                        <label for="preview_gambar">Pratinjau Gambar</label>
                        <img id="preview_gambar" src="#" alt="Gambar Produk" class="img-fluid"
                            style="display: none;" />
                    </div>
                    <div class="mb-3">
                        <label for="gambar" class="form-label">Gambar<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" id="gambar" name="gambar" required>
                    </div>
                    <div class="mb-3">
                        <label for="deskripsi" class="form-label">Deskripsi</label>
                        <input type="text" class="form-control" id="deskripsi" name="deskripsi">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit Product -->
<div class="modal fade" id="editSliderModal" tabindex="-1" aria-labelledby="editSliderModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editSliderModalLabel">Edit Slider</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="editSliderForm" action="/slider/{id}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="edit_nama_slider" class="form-label">Nama Slider</label>
                        <input type="text" class="form-control" id="edit_nama_slider" name="nama_slider" required>
                    </div>
                    <div class="form-group mb-3">
                        <label for="edit_preview_gambar">Pratinjau Gambar</label>
                        <img id="edit_preview_gambar" src="#" alt="Gambar Produk" class="img-fluid"
                            style="display: none;" />
                    </div>
                    <div class="mb-3">
                        <label for="edit_gambar" class="form-label">Gambar</label>
                        <input type="file" class="form-control" id="edit_gambar" name="gambar">
                    </div>
                    <div class="mb-3">
                        <label for="edit_deskripsi" class="form-label">Deskripsi</label>
                        <input type="text" class="form-control" id="edit_deskripsi" name="deskripsi">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
