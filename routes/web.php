<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ErrorController;
use App\Http\Controllers\HistoryController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\TopupController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['auth', 'role:admin'])->group(function () {
    Route::resource('/dashboard', AdminController::class)->names('admin');
    Route::resource('/pengguna', UsersController::class)->names('users');
    Route::resource('/produk', ProductController::class)->names('produk');
    Route::resource('/slider', SliderController::class)->names('slider');
    Route::resource('/kategori', CategoryController::class)->names('kategori');
    Route::resource('/pesanan', OrderController::class)->names('order');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [LoginController::class, 'logout'])->name('login.logout');
    Route::resource('/profil', ProfileController::class)->names('profile');
    Route::resource('/keranjang', CartController::class)->names('cart');
    Route::post('/barang/{id}', [CartController::class, 'store'])->name('cart.store');
    Route::post('/keranjang/{id}', [CartController::class, 'add'])->name('cart.add');
    Route::post('/keranjang/pay/{id}', [CartController::class, 'update'])->name('cart.update');
    // Route::get('/histori', [HistoryController::class, 'index'])->name('history.index');
    Route::resource('/histori', HistoryController::class)->names('history');
    // Route::get('/histori', [HistoryController::class, 'show'])->name('history.show');
    Route::resource('/topup', TopupController::class)->names('topup');
    // Route::get('/profil/topup', [TopupController::class, 'index'])->name('topup.index');
    Route::post('/profil/topup/{amount}', [TopupController::class, 'buyPackage'])->name('topup.buyPackage');
});

Route::resource('/login', LoginController::class)->names('login');
Route::resource('/daftar', RegisterController::class)->names('register');

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/barang', [HomeController::class, 'list'])->name('products.home');
Route::get('/barang/{id}', [HomeController::class, 'show'])->name('products.show');

Route::get('/error', [ErrorController::class, 'error'])->name('error.error');
Route::get('/cant', [ErrorController::class, 'cant'])->name('error.cant');
Route::get('/inprogress', [ErrorController::class, 'inprogress'])->name('error.inprogress');
