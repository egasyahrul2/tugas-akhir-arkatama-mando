<?php

namespace Database\Seeders;


use App\Models\Slider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataSliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $SlideData =[
            [
                'nama_slider'=>'Welcome Slide',
                'gambar'=>'Slider\4.png',
                'deskripsi'=>'ini Welcome Slide',
                'created_by'=>'1',
                'created_at'=> now()
            ],
            [
                'nama_slider'=>'Promo Terbaru',
                'gambar'=>'Slider\5.png',
                'deskripsi'=>'ini Promo Terbaru',
                'created_by'=>'1',
                'created_at'=> now()
            ],
            [
                'nama_slider'=>'Promo Tahun Baru',
                'gambar'=>'Slider\6.png',
                'deskripsi'=>'ini Promo Tahun Baru',
                'created_by'=>'1',
                'created_at'=> now()
            ],
        ];
        DB::table('slider')->insert($SlideData);
    }
}
