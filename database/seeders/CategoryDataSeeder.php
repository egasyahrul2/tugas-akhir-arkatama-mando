<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $CategoryData =[
            [
                'nama_kategori'=>'Backpack',
                'deskripsi'=>'ini kategori tas punggung',
                'created_by'=>'1',
                'created_at'=> now()
            ],
            [
                'nama_kategori'=>'Sling Bag',
                'deskripsi'=>'ini kategori tas Selempang',
                'created_by'=>'1',
                'created_at'=> now()
            ],
            [
                'nama_kategori'=>'Waist Bag',
                'deskripsi'=>'ini kategori tas pinggang',
                'created_by'=>'1',
                'created_at'=> now()
            ],
            [
                'nama_kategori'=>'Tote Bag',
                'deskripsi'=>'ini kategori tas belanja',
                'created_by'=>'1',
                'created_at'=> now()
            ],
            [
                'nama_kategori'=>'Clutch',
                'deskripsi'=>'ini kategori tas kecil',
                'created_by'=>'1',
                'created_at'=> now()
            ],
            [
                'nama_kategori'=>'Aksesori',
                'deskripsi'=>'ini kategori aksesori tas',
                'created_by'=>'1',
                'created_at'=> now()
            ],
        ];
        DB::table('categories')->insert($CategoryData);
    }
}
