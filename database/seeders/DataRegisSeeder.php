<?php

namespace Database\Seeders;

use App\Models\Register;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataRegisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $RegisData =[
            [
                'id_user'=>1,
                'name'=>'Admin',
                'email'=>'admin@gmail.com',
                'alamat'=>'Alamat Admin',
                'saldo'=>'500000',
                'is_active'=>1,
                'created_by'=>1,
                'created_at'=> now()
            ],
            [
                'id_user'=>2,
                'name'=>'Pembeli 1',
                'email'=>'user1@gmail.com',
                'alamat'=>'Alamat Pembeli 1',
                'saldo'=>'500000',
                'is_active'=>1,
                'created_by'=>1,
                'created_at'=> now()
            ],
            [
                'id_user'=>3,
                'name'=>'user',
                'email'=>'user@gmail.com',
                'alamat'=>'Alamat user',
                'saldo'=>'500000',
                'is_active'=>1,
                'created_by'=>1,
                'created_at'=> now()
            ],
        ];
        DB::table('registration')->insert($RegisData);
    }
}
