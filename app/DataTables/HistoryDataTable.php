<?php

namespace App\DataTables;

use App\Models\Cart;
use App\Models\History;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class HistoryDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'history.action')
            ->addIndexColumn()
            ->addColumn('gambar', function (Cart $row) {
                return '<img src="' . asset('storage/' . $row->product->gambar) . '" alt="' . $row->product->nama_produk . '" class="img-fluid" width="50">';
            })
            ->addColumn('nama', function (Cart $row) {
                return $row->product->nama_produk;
            })
            ->addColumn('user', function (Cart $row) {
                return $row->users->name;
            })
            ->addColumn('total', function (Cart $row) {
                return 'Rp ' . number_format($row->total, 0, ',', '.');
            })
            ->editColumn('created_at', function (Cart $row) {
                return $row->created_at->format('d-m-Y H:i:s');
            })
            ->editColumn('updated_at', function (Cart $row) {
                return $row->updated_at->format('d-m-Y H:i:s');
            })
            ->rawColumns(['gambar'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Cart $model): QueryBuilder
    {
        return $model->newQuery()
            ->with('product')
            ->with('users')
            ->where('status', '2')
            ->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('history-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addTableClass('table table-row-dashed  gy-5 dataTable no-footer text-gray-600 fw-semibold fs-5')
            ->setTableHeadClass('thead fw-bold text-uppercase')
            ->select(false)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::computed('DT_RowIndex')
                ->title('No')
                ->orderable(false)
                ->addClass('text-center')
                ->searchable(false),
            Column::computed('user')
                ->title('Nama Pelanggan')
                ->searchable(true),
            Column::computed('nama')
                ->title('Nama Produk')
                ->searchable(true),
            Column::computed('gambar')
                ->title('Gambar')
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->addClass('text-center'),
            Column::make('qty')
                ->title("Jumlah")
                ->searchable(true)
                ->addClass('text-center'),
            Column::computed('total')
                ->title("Harga")
                ->searchable(true),
            Column::make('created_at')
                ->title("Order Date")
                ->searchable(true),
            Column::make('updated_at')
                ->title("Confirm Date")
                ->searchable(true),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'History_' . date('YmdHis');
    }
}
