<?php

namespace App\DataTables;

use App\Models\Slider;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SliderDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('aksi', 'slider.action')
            ->addIndexColumn()
            ->addColumn('gambar', function (Slider $row) {
                $path = asset('storage/'.$row->gambar);
                return '<img src="' . $path . '" alt="Gambar" height="80">';
            })
            ->addColumn('aksi', function (Slider $row) {
                return view('pages.admin.slider.action', ['data' => $row]);
            })
            ->rawColumns(['aksi', 'gambar'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Slider $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('slider-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->orderBy('0', 'desc')
            ->addTableClass('table table-row-dashed  gy-5 dataTable no-footer text-gray-600 fw-semibold fs-5')
            ->setTableHeadClass('thead fw-bold text-uppercase')
            ->select(false)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id')
                ->visible(false)
                ->searchable(false),
            Column::computed('aksi')
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->width(10)
                ->addClass('text-center'),
            Column::make('nama_slider')
                ->title("Nama Slider")
                ->searchable(true),
            Column::computed('gambar')
                ->title("Gambar")
                ->searchable(false)
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
            Column::make('deskripsi')
                ->title("Deskripsi")
                ->searchable(true),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Slider_' . date('YmdHis');
    }
}
