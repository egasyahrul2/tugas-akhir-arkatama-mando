<?php

namespace App\DataTables;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CartDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'cart.action')
            ->addIndexColumn()
            ->addColumn('aksi', function (Cart $row) {
                if ($row->status == 0) {
                    return view('pages.user.cart.action', ['data' => $row]);
                } elseif ($row->status == 1) {
                    return '<a href="https://wa.me/6282331879753?text=Halo, saya telah melakukan pembayaran dan menunggu konfirmasi." target="_blank" class="btn btn-success"><i class="fab fa-whatsapp"></i></a>';
                }
            })
            ->addColumn('gambar', function (Cart $row) {
                return '<img src="' . asset('storage/' . $row->product->gambar) . '" alt="' . $row->product->nama_produk . '" class="img-fluid" width="50">';
            })
            ->addColumn('nama', function (Cart $row) {
                return $row->product->nama_produk;
            })
            ->addColumn('total', function (Cart $row) {
                return 'Rp ' . number_format($row->total, 0, ',', '.');
            })
            ->addColumn('status', function (Cart $row) {
                if ($row->status == 0) {
                    return '<button class="btn btn-danger">Belum Bayar</button>';
                } elseif ($row->status == 1) {
                    return '<button class="btn btn-warning">Pending</button>';
                }
            })
            ->rawColumns(['aksi', 'gambar', 'status'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Cart $model): QueryBuilder
    {
        return $model->newQuery()
            ->with('product')
            ->where('id_user', auth()->id())
            ->whereIn('status', ['0', '1'])
            ->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('cart-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addTableClass('table table-row-dashed  gy-5 dataTable no-footer text-gray-600 fw-semibold fs-5')
            ->setTableHeadClass('thead fw-bold text-uppercase')
            ->select(false)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::computed('aksi')
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->width(10)
                ->addClass('text-center'),
            Column::computed('gambar')
                ->title('Gambar')
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->addClass('text-center'),
            Column::computed('nama')
                ->title('Nama Produk')
                ->searchable(true),
            Column::make('qty')
                ->title("Jumlah")
                ->searchable(true)
                ->addClass('text-center'),
            Column::computed('total')
                ->title("Harga")
                ->searchable(false),
            Column::computed('status')
                ->title("Status")
                ->searchable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Cart_' . date('YmdHis');
    }
}
