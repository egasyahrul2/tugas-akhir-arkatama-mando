<?php

namespace App\DataTables;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('aksi', 'product.action')
            ->addIndexColumn()
            ->addColumn('gambar', function (Product $row) {
                $path = asset('storage/' . $row->gambar);
                return '<img src="' . $path . '" alt="Gambar" height="80">';
            })
            ->addColumn('aksi', function (Product $row) {
                return view('pages.admin.product.action', ['data' => $row]);
            })
            ->addColumn('nama_kategori', function (Product $row) {
                return $row->category->nama_kategori;
            })
            ->addColumn('harga', function (Product $row) {
                return 'Rp ' . number_format($row->harga, 0, ',', '.');
            })
            ->rawColumns(['aksi', 'gambar'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Product $model): QueryBuilder
    {
        return $model->newQuery()->with('category');
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('product-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->orderBy('0', 'desc')
            ->addTableClass('table table-row-dashed  gy-5 dataTable no-footer text-gray-600 fw-semibold fs-5')
            ->setTableHeadClass('thead fw-bold text-uppercase')
            ->select(false)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id')
                ->visible(false)
                ->searchable(false),
            Column::computed('aksi')
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->width(10)
                ->addClass('text-center'),
            Column::make('nama_produk')
                ->title("Nama Produk")
                ->searchable(true),
            Column::computed('nama_kategori')
                ->title("Nama Kategori")
                ->searchable(true),
            Column::computed('gambar')
                ->title("Gambar")
                ->searchable(false)
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
            Column::make('qty')
                ->title("QTY")
                ->searchable(true)
                ->addClass('text-center'),
            Column::computed('harga')
                ->title("Harga")
                ->searchable(false),
            Column::make('deskripsi')
                ->title("Deskripsi")
                ->searchable(true),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Product_' . date('YmdHis');
    }
}
