<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Models\Register;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use function Laravel\Prompts\confirm;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(UsersDataTable $datatable)
    {
        return $datatable->render('pages.admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.admin.users.modals.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validasi input
        $request->validate([
            'nama' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8',
            'alamat' => 'nullable|string|max:255',
            'role' => 'required|in:user,admin',
            // 'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        // if ($request->hasFile('gambar')) {
        //     $path = $request->file('gambar')->store('profile_images', 'public');
        // } else {
        //     $path = null;
        // }

        // Buat user baru di tabel users
        $user = User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->password),
            // 'gambar' => $path
        ]);

        Register::create([
            'id_user' => $user->id,
            'email' => $request->email,
            'name' => $request->nama,
            'alamat' => $request->alamat,
            'saldo' => 0,
        ]);

        return redirect()->route('users.index')->with('success', 'Data pengguna berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $regis = Register::find($id);
        $data = User::where('id', $regis->id_user)->first();
        return view('pages.admin.users.modals.detail', compact('data', 'regis'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email,' . $id,
            'alamat' => 'nullable|string|max:255',
            'role' => 'required|in:user,admin',
            // 'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $regis = Register::find($id);
        $user = User::where('id', $regis->id_user)->first();

        $user->name = $request->nama;
        $user->email = $request->email;
        $user->role = $request->role;

        if ($request->filled('password')) {
            $request->validate([
                'password' => 'required|string|min:8',
            ]);
            $user->password = Hash::make($request->password);
        }

        // if ($request->hasFile('gambar')) {
        //     if ($user->gambar) {
        //         Storage::disk('public')->delete($user->gambar);
        //     }
        //     $path = $request->file('gambar')->store('profile_images', 'public');
        //     $user->gambar = $path;
        // }

        $user->save();

        $regis->name = $request->nama;
        $regis->email = $request->email;
        $regis->alamat = $request->alamat;

        $regis->save();


        return redirect()->route('users.index')->with('success', 'Data pengguna berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $regis = Register::where('id_user', $id)->first();

        $regis->delete();

        $user = User::find($id);

        // if ($user->gambar) {
        //     Storage::disk('public')->delete($user->gambar);
        // }

        $user->delete();

        return redirect()->route('users.index')->with('success', 'Data pengguna berhasil dihapus.');
    }
}
