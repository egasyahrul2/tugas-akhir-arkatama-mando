<?php

namespace App\Http\Controllers;

use App\DataTables\HistoryDataTable;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function index(HistoryDataTable $datatable)
    {
        return $datatable->render('pages.admin.histori.index');
    }

    public function show()
    {
        $user = Auth::user();
        $products = Cart::with('product')
            ->where('id_user', $user->id)
            ->where('status', '2')
            ->get();

        return view('pages.user.histori.index', compact('products'));
    }
}
