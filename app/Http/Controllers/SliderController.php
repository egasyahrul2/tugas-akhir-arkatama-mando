<?php

namespace App\Http\Controllers;

use App\DataTables\SliderDataTable;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(SliderDataTable $datatable)
    {
        return $datatable->render('pages.admin.slider.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_slider' => 'required|string|max:255',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            'deskripsi' => 'nullable|string',
        ]);

        if ($request->hasFile('gambar')) {
            $filePath = $request->file('gambar')->store('Slider', 'public');
        }

        Slider::create([
            'nama_slider' => $request->nama_slider,
            'gambar' => $filePath,
            'deskripsi' => $request->deskripsi,
        ]);

        // Slider::create($request->all());

        return response()->json(['success' => 'Data berhasil Ditambahkan']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Slider::findOrFail($id);
        $data->gambar_url = asset('storage/' . $data->gambar);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
                'nama_slider' => 'required|string|max:255',
                'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                'deskripsi' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()]);
        }

        $data = Slider::findOrFail($id);

        if ($request->hasFile('gambar')) {
            if ($data->gambar) {
                Storage::disk('public')->delete($data->gambar);
            }
            $filePath = $request->file('gambar')->store('Slider', 'public');
            $data->gambar = $filePath;
        } else{
            $data->gambar = $data->gambar;
        }

        $data->nama_slider = $request->nama_slider;
        $data->deskripsi = $request->deskripsi;
        $data->save();

        return response()->json(['success' => 'Data berhasil diperbarui']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Slider::findOrFail($id);
            if ($data->gambar) {
                Storage::disk('public')->delete($data->gambar);
            }
        $data->delete();

        return response()->json(['success' => 'Data berhasil dihapus']);
    }
}
