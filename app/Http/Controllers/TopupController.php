<?php

namespace App\Http\Controllers;

use App\Models\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopupController extends Controller
{
    public function index() {
        return view('pages.user.profil.topup');
    }

    public function buyPackage($amount) {
        $user = Auth::user();
        $registration = Register::where('id_user', $user->id)->first();

        if ($registration) {
            $registration->saldo += $amount;
            $registration->save();

            return redirect()->route('profile.index')->with('success', 'Saldo berhasil ditambahkan.');
        }

        return redirect()->route('topup.index')->with('error', 'Gagal menambahkan saldo.');
    }
}
