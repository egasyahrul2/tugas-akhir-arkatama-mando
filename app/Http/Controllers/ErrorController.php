<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function error() {
        return view('komponen.modal.error');
    }

    public function cant() {
        return view('komponen.modal.cant');
    }

    public function inprogress() {
        return view('komponen.modal.inprogress');
    }
}
