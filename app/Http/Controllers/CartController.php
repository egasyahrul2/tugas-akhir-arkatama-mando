<?php

namespace App\Http\Controllers;

use App\DataTables\CartDataTable;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Register;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index(CartDataTable $datatable)
    {
        return $datatable->render('pages.user.cart.index');
    }

    public function store(Request $cartItem, $id)
    {
        $userId = Auth::id();
        $product = Product::findOrFail($id);

        if ($product->qty == 0) {
            return redirect()->back()->with('error', 'Maaf, produk ini telah habis.');
        }

        $cartItem = Cart::where('id_user', $userId)->where('id_product', $id)->first();

        if ($cartItem) {
            $cartItem->qty += 1;
            $cartItem->total += $product->harga;
            $cartItem->save();
        } else {
            Cart::create([
                'id_user' => $userId,
                'id_product' => $product->id,
                'qty' => 1,
                'total' => $product->harga,
            ]);
        }

        $product->qty -= 1;
        $product->save();

        return redirect()->back()->with('success', 'Produk berhasil ditambahkan ke keranjang.');
    }

    public function update(Request $request, $id)
    {
        $userId = Session::get('user_id');
        $cartItem = Cart::where('id_user', $userId)->where('id', $id)->first();

        if (!$cartItem) {
            return redirect()->back()->with('error', 'Produk tidak ditemukan di keranjang.');
        }

        $user = Register::where('id_user', $userId)->first();
        $total = $cartItem->total;

        if ($user->saldo < $total) {
            return redirect()->back()->with('error', 'Saldo tidak mencukupi untuk melakukan pembayaran.');
        }

        $user->saldo -= $total;
        $user->save();
        $cartItem->status = '1';
        $cartItem->save();

        return redirect()->route('cart.index')->with('success', 'Pembayaran berhasil.');
    }

    public function destroy($id)
    {
        $cartItem = Cart::findOrFail($id);

        if (!$cartItem) {
            return redirect()->back()->with('error', 'Produk tidak ditemukan di keranjang.');
        }

        $product = Product::where('id', $cartItem->id_product)->first();

        if ($cartItem->qty > 1) {
            $cartItem->qty -= 1;
            $cartItem->total -= $product->harga;
            $cartItem->save();
        } else {
            $cartItem->delete();
        }

        $product->qty += 1;
        $product->save();

        return redirect()->route('cart.index')->with('success', 'Produk berhasil dikurangi dari keranjang.');
    }

    public function add($id)
    {
        $cartItem = Cart::findOrFail($id);

        if (!$cartItem) {
            return redirect()->back()->with('error', 'Produk tidak ditemukan di keranjang.');
        }

        $product = Product::where('id', $cartItem->id_product)->first();

        if ($cartItem->qty != 0) {
            $cartItem->qty += 1;
            $cartItem->total += $product->harga;
            $cartItem->save();
        } else {
            return redirect()->back()->with('error', 'Produk tidak ditemukan di keranjang.');
        }

        $product->qty -= 1;
        $product->save();

        return redirect()->route('cart.index')->with('success', 'Produk berhasil ditambahkan dari keranjang.');
    }
}
