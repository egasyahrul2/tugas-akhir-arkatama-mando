<?php

namespace App\Http\Controllers;

use App\DataTables\OrderDataTable;
use App\Models\Cart;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(OrderDataTable $datatable)
    {
        return $datatable->render('pages.admin.pesanan.index');
    }

    public function update(Request $request, string $id)
    {
        $data = Cart::where('id', $id)->first();

        if (!$data) {
            return redirect()->back()->with('error', 'Pesanan tidak ditemukan');
        }

        $data->status = '2';
        $data->save();

        return redirect()->route('order.index')->with('success', 'Pesanan di berhasil dikonfirmasi.');
    }
}
