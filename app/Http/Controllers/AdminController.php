<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\User;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pending = Cart::where('status', '1')->count();
        $totalLaba = Cart::where('status', '2')->sum('total');
        $totalUsers = User::where('role', 'user')->count();
        $totalProducts = Product::count();
        $totalBarangTerjual = Cart::where('status', '2')->sum('qty');

        return view('pages.admin.index', compact(
            'pending',
            'totalLaba',
            'totalUsers',
            'totalProducts',
            'totalBarangTerjual',
        ));
    }
}
