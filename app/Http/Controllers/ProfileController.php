<?php

namespace App\Http\Controllers;

use App\Models\Register;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // $data = User::find($id);
        // if (!$data) {
        //     abort(404);
        // } else{
        //     $id_user = $id;
        //     $sync = Register::find($id_user);
        // }
        // return view('pages.profil.index', compact('data', 'sync'));
        $userId = Session::get('user_id');

        // if (!$userId) {
        //     return redirect()->route('login');
        // }

        $user = Register::where('id_user', $userId)->first();

        if (!$user) {
            return redirect()->route('login');
        }

        return view('pages.user.profil.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
