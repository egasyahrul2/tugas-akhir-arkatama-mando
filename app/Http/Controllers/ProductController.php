<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\DataTables\ProductDataTable;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(ProductDataTable $datatable)
    {
        $categories = Category::all();
        return $datatable->render('pages.admin.product.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required|string|max:255',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            'qty' => 'required|numeric',
            'harga' => 'required|string',
            'warna' => 'required|string',
            'deskripsi' => 'nullable|string',
            'id_kategori' => 'required|exists:categories,id'
        ]);

        if ($request->hasFile('gambar')) {
            $filePath = $request->file('gambar')->store('Product', 'public');
        }

        Product::create([
            'nama_produk' => $request->nama_produk,
            'gambar' => $filePath,
            'qty' => $request->qty,
            'harga' => $request->harga,
            'warna' => $request->warna,
            'deskripsi' => $request->deskripsi,
            'id_kategori' => $request->id_kategori
        ]);

        return response()->json(['success' => 'Data berhasil Ditambahkan']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $product->gambar_url = asset('storage/' . $product->gambar);
        $categories = Category::all();
        return response()->json(['product' => $product, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_produk' => 'required|string|max:255',
            'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            'qty' => 'required|numeric',
            'harga' => 'required|string',
            'warna' => 'required|string',
            'deskripsi' => 'nullable|string',
            'id_kategori' => 'required|exists:categories,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()]);
        }

        $data = Product::findOrFail($id);

        if ($request->hasFile('gambar')) {
            if ($data->gambar) {
                Storage::disk('public')->delete($data->gambar);
            }
            $filePath = $request->file('gambar')->store('Product', 'public');
            $data->gambar = $filePath;
        } else{
            $data->gambar = $data->gambar;
        }

        $data->nama_produk = $request->nama_produk;
        $data->qty = $request->qty;
        $data->harga = $request->harga;
        $data->warna = $request->warna;
        $data->deskripsi = $request->deskripsi;
        $data->id_kategori = $request->id_kategori;
        $data->save();

        return response()->json(['success' => 'Data berhasil diperbarui']);
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Product::findOrFail($id);

        if ($data->gambar) {
            Storage::disk('public')->delete($data->gambar);
        }

        $data->delete();


        return response()->json(['success' => 'Data berhasil dihapus']);
    }
}
