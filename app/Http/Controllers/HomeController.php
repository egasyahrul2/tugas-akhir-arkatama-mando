<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Slider;

class HomeController extends Controller
{
    function index()
    {
        $slide = Slider::all();
        $data = Product::latest()->take(20)->get();
        return view('index', compact('data', 'slide'));
    }

    public function show($id)
    {
        $data = Product::find($id);
        if (!$data) {
            abort(404);
        }
        return view('pages.user.detail', compact('data'));
    }

    public function list(Request $request)
    {
        $query = Product::with('category');

        if ($request->has('search')) {
            $search = $request->get('search');
            $query->where(function ($q) use ($search) {
                $q->where('nama_produk', 'LIKE', "%{$search}%")
                    ->orWhere('deskripsi', 'LIKE', "%{$search}%")
                    ->orWhere('harga', 'LIKE', "%{$search}%")
                    ->orWhere('warna', 'LIKE', "%{$search}%")
                    ->orWhereHas('category', function ($q) use ($search) {
                        $q->where('nama_kategori', 'LIKE', "%{$search}%");
                    });
            });
        }

        $data = $query->get();
        return view('pages.user.list', compact('data'));
    }
}
