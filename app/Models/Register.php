<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    use HasFactory;
    protected $table = 'registration';
    protected $guarded = ['id'];

    protected $fillable = [
        'id_user',
        'name',
        'email',
        'alamat',
        'saldo',
        'is_active'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
