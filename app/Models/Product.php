<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'product';
    protected $guarded = ['id'];

    protected $fillable = [
        'nama_produk',
        'gambar',
        'qty',
        'harga',
        'warna',
        'deskripsi',
        'id_kategori',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'id_kategori');
    }
}
