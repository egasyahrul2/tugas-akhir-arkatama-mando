<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $table = 'carts';
    protected $guarded = ['id'];

    protected $fillable = [
        'id_user',
        'id_product',
        'qty',
        'total',
        'status',
        'created_at',
        'updated_at',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
